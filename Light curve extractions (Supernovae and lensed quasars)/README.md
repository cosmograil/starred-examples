# Light curves extraction notebooks

These notebooks reproduce the results presented in Section 4.1 and 4.2 of Millon et al.(2024). They can be used to generate both the SNIa light curve and the lensed quasar light curves. They require to install [lenstronomy](https://github.com/lenstronomy) to generate the mock images. 

1. 1_generate_simulation.ipynb : generate a test set, containing mock observations
2. 2_run_psf_reconstruction.ipynb : run the PSF reconstruction on the simulated stars. PSF is reconstructed at each epoch. 
3. 3_run_deconvolution.ipynb : run the deconvolution and extract the flux of the point sources at each epoch
4. 3b_estimate_errors : demonstrate alternative way of computing flux uncertainties
5. 4_plot_results.ipynb : Check the results, compare with the input light curves. Reproduce Fig. 5.,6. and 8 of Millon et al. (2024)
6. 5a_summarize_SNIa.ipynb : Measure accuracy and precision for the SNIa simulation. Reproduce Table 1. 
7. 5b_summarize_lensed_quasar.ipynb : Measure accuracy and precision for the lensed quasar simulation. Reproduce Table 2. and Figure 7.

You can run the third and 4th notebook sequentially for all SNIa location or Einstein radii with : 
1. runner_3_deconv.ipynb
2. runner_4_plot_results.ipynb

