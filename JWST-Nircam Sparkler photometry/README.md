# JWST point source photometry notebooks 

These notebooks reproduce the results presented in Section 4.3 of Millon et al.(2024). They should be run on each photometric bands. 

1. (optional) 1_Extract_PSF_sparkler.ipynb : this notebook extract the cutouts for the stars used for estimating the PSF and for the Sparkler
2. 2_Reconstruct_PSF.ipynb : run the PSF reconstruction routine of STARRED
3. 3_JWST_photometry.ipynb : run the deconvolution and extract the flux of the point sources
4. 4_summarize.ipynb : Produce the SED plots and final color images (reproduce Figure 9,10 and Table 3 of Millon et al. (2024)). 


You can run the 3 first notebooks sequentially on all the bands by calling: 
1. runner_1_extract.ipynb
2. runner_2_psf.ipynb
3. runner_3_deconv.ipynb
