from utils_plots import prepare_image, choose_color
from astropy.io import fits 
import os 
import matplotlib.pyplot as plt
import numpy as np 

lambda_scales = 1
lambda_hf = 100
regularize_pts_source = False
noise_map_type = 'iweight'
subsampling_factor =2 
suf = ''
if regularize_pts_source:
    suf+= 'regpts'
deconv_name = 'deconv_lambda%2.2f_%2.2f_%s_sub%i_%s'%(lambda_scales, lambda_hf, noise_map_type, subsampling_factor,suf)


# RGB image
r_band = 'f200w'
g_band = 'f150w'
b_band = 'f090w'
framesize = 200

select_color_cut = True

data_file = os.path.join('./', r_band, 'deconvolution', deconv_name, 'data_00000.fits')
deconv_file = os.path.join('./', r_band, 'deconvolution', deconv_name, 'deconvolution_00000.fits')
R = fits.open(data_file)[0].data
R_dec = fits.open(deconv_file)[0].data
im_size, _ = np.shape(R)
im_size_up, _ = np.shape(R_dec)

data_file = os.path.join('./', g_band, 'deconvolution', deconv_name, 'data_00000.fits')
deconv_file = os.path.join('./', g_band, 'deconvolution', deconv_name, 'deconvolution_00000.fits')
G = fits.open(data_file)[0].data
G_dec = fits.open(deconv_file)[0].data

data_file = os.path.join('./', b_band, 'deconvolution', deconv_name, 'data_00000.fits')
deconv_file = os.path.join('./', b_band, 'deconvolution', deconv_name, 'deconvolution_00000.fits')
B = fits.open(data_file)[0].data
B_dec = fits.open(deconv_file)[0].data

#select color cuts: 
coef_a = [-0.8205128205128203, -1.4647435897435894, -1.8974358974358974] 
coef_m = [3.0235042735042734, 3.9209401709401694, 4.113247863247863]
coef_a_dec = [-0.8205128205128203, -1.4647435897435894, -1.8974358974358974] 
coef_m_dec= [3.0235042735042734, 3.9209401709401694, 4.113247863247863]

R_dec *= subsampling_factor**2
G_dec *= subsampling_factor**2
B_dec *= subsampling_factor**2


rgb_data, init_offset, a_init, b_init = prepare_image(R, G, B, framesize=im_size, ref=True)
print(init_offset)
rgb_dec, _, _, _ = prepare_image(R_dec, G_dec, B_dec, framesize=im_size_up, ref=False, init_offset=init_offset, a_init=a_init, b_init=b_init)

print(init_offset, a_init, b_init)

if select_color_cut: 
    coef_a, coef_m = choose_color(rgb_data, coef_a, coef_m,  plot_hist = False)
    print('New color cut : ', coef_a, coef_m)

    coef_a_dec, coef_m_dec = choose_color(rgb_dec, coef_a, coef_m,  plot_hist = False)
    print('New color cut dec : ', coef_a_dec, coef_m_dec)

plt.show() 