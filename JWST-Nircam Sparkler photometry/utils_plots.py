import copy 
import matplotlib.pyplot as plt 
import numpy as np 
from matplotlib.widgets import Slider

def affine_coef(dis):
    return 1 / (np.max(dis) - np.min(dis)), -np.min(dis)/(np.max(dis) - np.min(dis))

def affine_transform(dis, a,b):
    return a*dis + b

def prepare_image(R,G,B, framesize = 200, ref = True, init_offset = None, a_init = None, b_init = None):
    #prepare RGB data cube, remove nan, and affine transform to 0->1

    rgb = np.dstack((R,G,B))
    if init_offset is None :
        init_offset = []

    for i in range(3):
        if ref:
            init_offset.append(np.min(rgb))
            rgb[:,:,i] -= np.min(rgb)
        else :
            rgb[:, :, i] -= init_offset[i]

    rgb = np.log10(rgb)
    rgb[np.isnan(rgb)] = np.nanmin(rgb)
    rgb[np.isinf(rgb)] = 0

    label = ['R','G','B']

    if a_init is None :
        a_init = []
    if b_init is None :
        b_init = []

    for i in range(3):
        if ref :
            a,b = affine_coef(rgb[:, :, i])
            a_init.append(a)
            b_init.append(b)
        else :
            a,b = a_init[i], b_init[i]

        rgb[:, :, i] = affine_transform(rgb[:, :, i], a,b )

    x,y,z = np.shape(rgb)

    if x < framesize:
        #padding
        new_frame = np.zeros((framesize, framesize, 3))
        margin = int((framesize - x) /2.)
        new_frame[margin:-margin, margin:-margin, :] = rgb
        rgb = copy.deepcopy(new_frame)

    elif x > framesize:
        #cutting
        margin = int((x - framesize) / 2.)
        rgb = rgb[margin:-margin, margin:-margin, :]

    return rgb, init_offset, a_init, b_init

def pad_im2d(im, framesize):
    x, y = np.shape(im)
    if x < framesize:
        #padding
        new_frame = np.zeros((framesize, framesize, 2))
        margin = int((framesize - x) /2.)
        new_frame[margin:-margin, margin:-margin] = im
        im = copy.deepcopy(new_frame)

    elif x > framesize:
        #cutting
        margin = int((x - framesize) / 2.)
        im = im[margin:-margin, margin:-margin]

    return im


def choose_color(rgb, coef_a, coef_m,  plot_hist = False, framesize = 200):
    '''
    Interactive plotting function to choose the color cuts. 

    :param rgb: RGB data cube 
    :param coef_a: Dimension 3 array, containing the RGB additive factor 
    :param coef_m: Dimension 3 array, containing the RGB multiplicative factor 
    :param plot_hist: Option to plot pixel histogram 
    '''

    label = ['R','G','B']

    original_rgb = copy.deepcopy(rgb)

    if plot_hist :
        for i in range(3):
            plt.figure(i)
            plt.hist(np.ravel(original_rgb[:,:,i]), bins =200)
            plt.title(label[i])

    def update(val):
        print('update')
        aR = s_aR.val
        mR = s_mR.val
        aG = s_aG.val
        mG = s_mG.val
        aB = s_aB.val
        mB = s_mB.val
        rgb[:, :, 0] = mR * original_rgb[:, :, 0] + aR
        rgb[:, :, 1] = mG * original_rgb[:, :, 1] + aG
        rgb[:, :, 2] = mB * original_rgb[:, :, 2] + aB
        im.set_data(rgb)
        fig.canvas.draw_idle()

    fig = plt.figure(4, figsize=(12,12))
    ax = fig.add_subplot(111)
    fig.subplots_adjust(bottom=0.1, top=0.6)
    # Create axes for sliders
    ax_aR = fig.add_axes([0.3, 0.65, 0.4, 0.05])
    ax_aR.spines['top'].set_visible(True)
    ax_aR.spines['right'].set_visible(True)
    ax_mR = fig.add_axes([0.3, 0.7, 0.4, 0.05])
    ax_mR.spines['top'].set_visible(True)
    ax_mR.spines['right'].set_visible(True)

    ax_aG = fig.add_axes([0.3, 0.75, 0.4, 0.05])
    ax_aG.spines['top'].set_visible(True)
    ax_aG.spines['right'].set_visible(True)
    ax_mG = fig.add_axes([0.3, 0.8, 0.4, 0.05])
    ax_mG.spines['top'].set_visible(True)
    ax_mG.spines['right'].set_visible(True)

    ax_aB = fig.add_axes([0.3, 0.85, 0.4, 0.05])
    ax_aB.spines['top'].set_visible(True)
    ax_aB.spines['right'].set_visible(True)
    ax_mB = fig.add_axes([0.3, 0.9, 0.4, 0.05])
    ax_mB.spines['top'].set_visible(True)
    ax_mB.spines['right'].set_visible(True)

    s_aR = Slider(ax=ax_aR, label='bias R', valmin=-3., valmax=1,
                valinit=coef_a[0], valfmt='%2.2f', facecolor='#cc7000')
    s_mR = Slider(ax=ax_mR, label='mult R', valmin=0, valmax=10,
                 valinit=coef_m[0], valfmt='%2.2f', facecolor='#cc7000')

    s_aG = Slider(ax=ax_aG, label='bias G', valmin=-3, valmax=1.0,
                valinit=coef_a[1], valfmt='%2.2f', facecolor='#cc7000')
    s_mG = Slider(ax=ax_mG, label='mult G', valmin=0, valmax=10,
                 valinit=coef_m[1], valfmt='%2.2f', facecolor='#cc7000')

    s_aB = Slider(ax=ax_aB, label='bias B', valmin=-3., valmax=1.0,
                valinit=coef_a[2], valfmt='%2.2f', facecolor='#cc7000')
    s_mB = Slider(ax=ax_mB, label='mult B', valmin=0, valmax=10,
                 valinit=coef_m[2], valfmt='%2.2f', facecolor='#cc7000')

    im = ax.imshow(rgb, origin='lower')
    update(1)
    s_aR.on_changed(update)
    s_mR.on_changed(update)
    s_aG.on_changed(update)
    s_mG.on_changed(update)
    s_aB.on_changed(update)
    s_mB.on_changed(update)

    plt.show()
    coef_a = [s_aR.val, s_aG.val,s_aB.val]
    coef_m = [s_mR.val, s_mG.val,s_mB.val]
    return coef_a, coef_m