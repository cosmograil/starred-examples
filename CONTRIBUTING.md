# Contributing

You can contribute to STARRED Example by proposing your own example notebook. Every contribution is 
greatly appreciated. 

## Reporting issues or problems

Please use the main STARRED repository [GitLab issues](https://gitlab.com/cosmograil/starred/-/issues) to report bugs. To do so, please include:

* The name and version of your operating system.
* Detailed steps to reproduce the bug.

## Implementing new features 

Please use [merge requests](https://gitlab.com/cosmograil/starred-examples/-/merge_requests) if you want to modify the master branch.

## Reaching out to code developers

The list of authors and their email addresses are available [here](https://gitlab.com/cosmograil/starred-examples/-/blob/main/AUTHORS.md).
