# PSF tests notebooks 

These notebooks reproduce the tests presented in Section 3 of Millon et al. (2024). 

1. run 1_test_generator.ipynb to generate a test set, containing mock star observations
2. run 2_run_psf_reconstruction.ipynb to run the PSF reconstruction routine of STARRED
3. run 3_plot_metrics.ipynb to extract the results
4. run 4_plot_metrics.ipynb to make the final performance plots (reproduce Figure 4 of Millon et al. (2024)). 
