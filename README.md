# STARRED Examples

This repository gathers advanced example notebooks that illustrate how [STARRED](https://gitlab.com/cosmograil/starred) can be used for different 
science cases. [Basic examples](https://gitlab.com/cosmograil/starred/-/tree/main/notebooks?ref_type=heads) can be found in the main [STARRED](https://gitlab.com/cosmograil/starred) repository.

## Installation 

You will need to install [STARRED](https://gitlab.com/cosmograil/starred) to run these notebooks. To install the latest version use `pip`:

```bash
$ pip install starred-astro
```

## Index
### HST tinyTim PSF reconstruction
These notebooks reproduce the tests presented in Section 3 of Millon et al. (2024). 
1. [Generate test set](https://gitlab.com/cosmograil/starred-examples/-/blob/main/HST%20Tinytim%20PSF%20reconstruction/1_test_generator.ipynb?ref_type=heads)
2. [Run_psf_reconstruction](https://gitlab.com/cosmograil/starred-examples/-/blob/main/HST%20Tinytim%20PSF%20reconstruction/2_run_psf_reconstruction.ipynb?ref_type=heads)(requires PSFr and photutils)
3. [Extract results](https://gitlab.com/cosmograil/starred-examples/-/blob/main/HST%20Tinytim%20PSF%20reconstruction/3_extract_metrics.ipynb?ref_type=heads) 
4. [Plot performance metrics](https://gitlab.com/cosmograil/starred-examples/-/blob/main/HST%20Tinytim%20PSF%20reconstruction/4_plot_metrics.ipynb?ref_type=heads)

### Supernovae and lensed quasar light curve extraction
These notebooks reproduce the examples presented in Millon et al. (2024). The first notebook features the generation of mock LSST images using empirical PSF for the ESO/2m2 telescope. The second notebook reconstructs the narrow PSF used for hte deconvolution. Notebook 3 deconvolves the images and extract the light curves. Notebook 4 measures the astrometric and photometric precision. 
* [Generate mock LSST images](https://gitlab.com/cosmograil/starred-examples/-/blob/main/Light%20curve%20extractions%20(Supernovae%20and%20lensed%20quasars)/1_generate_simulation.ipynb?ref_type=heads) (uses lenstronomy)
* [Reconstruct the PSF](https://gitlab.com/cosmograil/starred-examples/-/blob/main/Light%20curve%20extractions%20(Supernovae%20and%20lensed%20quasars)/2_run_psf_reconstruction.ipynb?ref_type=heads) 
* [Run the deconvolution](https://gitlab.com/cosmograil/starred-examples/-/blob/main/Light%20curve%20extractions%20(Supernovae%20and%20lensed%20quasars)/3_run_deconvolution.ipynb?ref_type=heads)
* [Analyse the results and plot the light curve](https://gitlab.com/cosmograil/starred-examples/-/blob/main/Light%20curve%20extractions%20(Supernovae%20and%20lensed%20quasars)/4_plot_results.ipynb?ref_type=heads)
* [Measure the photometric accuracy and precision (SNIa)](https://gitlab.com/cosmograil/starred-examples/-/blob/main/Light%20curve%20extractions%20(Supernovae%20and%20lensed%20quasars)/5a_summarize_SNIa.ipynb?ref_type=heads)
* [Measure the photometric accuracy and precision (lensed quasar)](https://gitlab.com/cosmograil/starred-examples/-/blob/main/Light%20curve%20extractions%20(Supernovae%20and%20lensed%20quasars)/5b_summarize_lensed_quasar.ipynb?ref_type=heads)

### JWST photometry extraction in the Sparkler galaxy
* [Extract the cutouts from JWST images](https://gitlab.com/cosmograil/starred-examples/-/blob/main/JWST-Nircam%20Sparkler%20photometry/1_Extract_PSF_sparkler.ipynb?ref_type=heads)(optional)
* [Reconstruct the PSF](https://gitlab.com/cosmograil/starred-examples/-/blob/main/JWST-Nircam%20Sparkler%20photometry/2_Reconstruct_PSF.ipynb?ref_type=heads) 
* [Run the deconvolution](https://gitlab.com/cosmograil/starred-examples/-/blob/main/JWST-Nircam%20Sparkler%20photometry/3_JWST_photometry.ipynb?ref_type=heads)
* [Analyse the results and plot the SEDs](https://gitlab.com/cosmograil/starred-examples/-/blob/main/JWST-Nircam%20Sparkler%20photometry/4_summarize.ipynb?ref_type=heads)

## Attribution
If you use the notebook presented in this repository please cite [Millon et al. 2024](https://arxiv.org/abs/2402.08725)
and [Michalewicz et al. 2023](https://joss.theoj.org/papers/10.21105/joss.05340) for the STARRED code.

## Contributions 
You can contribute to STARRED Example by proposing your own example notebook. Every contribution is 
greatly appreciated. 

## License
STARRED and the notebooks in the STARRED Examples repository are free software. You can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation.

STARRED is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without 
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details ([LICENSE.txt](LICENSE)).

